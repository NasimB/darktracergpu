#include "camera.h"

#define M_PI 3.14159265359

Camera::Camera() :
	position(0, 20, 80),
	direction(0, 0, -1)
{

}


Camera::~Camera()
{

}


void Camera::ComputeCornerDirections(const unsigned int width, const unsigned int height)
{
	const float fov = 45.0f;
	float aspectratio = width / float(height);
	float angle = tan(M_PI * 0.5f * fov / 180.0f);


	{
		float xx = -1.0f * angle * aspectratio;
		float yy = 1.0f * angle;
		zeroZero = Vector3(xx, yy, -1.0f).normalize();
	}


	{
		float xx = 1.0f * angle * aspectratio;
		float yy = 1.0f * angle;
		oneZero = Vector3(xx, yy, -1.0f).normalize();
	}


	{
		float xx = -1.0f * angle * aspectratio;
		float yy = -1.0f * angle;
		zeroOne = Vector3(xx, yy, -1.0f).normalize();
	}


	{
		float xx = 1.0f * angle * aspectratio;
		float yy = -1.0f * angle;
		oneOne = Vector3(xx, yy, -1.0f).normalize();
	}
}
