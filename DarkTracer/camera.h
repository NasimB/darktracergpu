#pragma once

#include "vector3.h"
#include "ray.h"

class Camera
{
public:
	Camera();
	~Camera();
	
	void ComputeCornerDirections(const unsigned int width, const unsigned int height);

	Vector3 position;
	Vector3 direction;

	Vector3 zeroZero;
	Vector3 oneZero;
	Vector3 zeroOne;
	Vector3 oneOne;
};
