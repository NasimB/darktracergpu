#include <Windows.h>
#include <string>

#include "SDL.h"
#include <GL/glew.h>
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "glew32.lib")

#define M_PI 3.14159265359

#define SPEED 3.0f

#include "shader.h"
#include "raytracer.h"

extern "C" _declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	SDL_Init(SDL_INIT_EVERYTHING);

	// OpenGL attributes
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	}
	
	SDL_Window *window = SDL_CreateWindow("DarkTracer GPU", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Raytracer::width, Raytracer::height, SDL_WINDOW_OPENGL);

	Raytracer* raytracer = new Raytracer(window);

	Uint64 tick_time = SDL_GetPerformanceCounter();
	const float frequency = static_cast<float>(SDL_GetPerformanceFrequency());
	
	bool complete = false;
	while (!complete)
	{
		Uint64 last_tick_time = tick_time;
		tick_time = SDL_GetPerformanceCounter();
		float deltaTime = static_cast<float>(tick_time - last_tick_time) / frequency;

		float fps = max(1.0f / deltaTime, 0.0f);

		char buff[128];
		snprintf(buff, sizeof(buff), "DarkTracer GPU 1.0 - Frametime: %.2f ms - FPS: %.2f FPS", deltaTime * 1000.0f, fps);
		SDL_SetWindowTitle(window, buff);

		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				complete = true;
				break;

			default:
				break;
			}
		}

		const Uint8 *state = SDL_GetKeyboardState(NULL);

		if (state[SDL_SCANCODE_ESCAPE])
		{
			break;
		}


		if (state[SDL_SCANCODE_LEFT])
		{
			raytracer->camera.position.x -= deltaTime * SPEED;
		}
		else if (state[SDL_SCANCODE_RIGHT]) 
		{
			raytracer->camera.position.x += deltaTime * SPEED;
		}

		if (state[SDL_SCANCODE_UP])
		{
			raytracer->camera.position.z -= deltaTime * SPEED;
		}
		else if (state[SDL_SCANCODE_DOWN])
		{
			raytracer->camera.position.z += deltaTime * SPEED;
		}

		if (state[SDL_SCANCODE_PAGEUP])
		{
			raytracer->camera.position.y += deltaTime * SPEED;
		}
		else if (state[SDL_SCANCODE_PAGEDOWN])
		{
			raytracer->camera.position.y -= deltaTime * SPEED;
		}

		raytracer->Render(deltaTime);
	}

	delete raytracer;
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
