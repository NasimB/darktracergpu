#pragma once

#include "vector3.h"
#include "ray.h"

enum Material {
	Standard_Material = 0,
	Chrome_Material,
	Emissive_Material
};

class Object
{
public:
	Object();
	~Object();

	virtual bool Intersects(Ray ray, float &near, Vector3 &hitpoint, Vector3 &normal) = 0;

	Vector3 position;
	Vector3 color;
	Material material;
};
