#include "plane.h"

Plane::Plane():
	normal(0, 1, 0)
{
}


Plane::Plane(Vector3 position, Vector3 normal)
	: normal(normal.normalize())
{
	this->position = position;
}


Plane::Plane(Vector3 position, Vector3 normal, Vector3 color)
	: normal(normal.normalize())
{
	this->position = position;
	this->color = color;
}


Plane::Plane(Vector3 position, Vector3 normal, Vector3 color, float radius)
	: radius(radius),
	normal(normal.normalize())
{
	this->position = position;
	this->color = color;
}


Plane::~Plane()
{
}


bool Plane::Intersects(Ray ray, float & near, Vector3 & hitpoint, Vector3 & normal)
{
	// assuming vectors are all normalized
	float denom = this->normal.dot(-ray.direction);
	if (denom > 1e-6) 
	{
		Vector3 p0l0 = ray.origin - position;
		float t = p0l0.dot(this->normal) / denom;
		if (t > 0.0f && t < near)
		{
			Vector3 intersection = ray.origin + ray.direction * t;
			if (radius > 0 && (intersection - position).length() > radius)
				return false;
			
			near = t;
			hitpoint = ray.origin + ray.direction * t;
			normal = this->normal;
			return true;
		}
	}
	return false;
}
