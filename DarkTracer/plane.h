#pragma once

#include "object.h"

class Plane : public Object
{
public:
	Plane();
	Plane(Vector3 position, Vector3 normal);
	Plane(Vector3 position, Vector3 normal, Vector3 color);
	Plane(Vector3 position, Vector3 normal, Vector3 color, float radius);
	~Plane();

	// Returns true if it intersects with the ray and is the closest to its origin, false otherwise.
	virtual bool Intersects(Ray ray, float &near, Vector3 &hitpoint, Vector3 &normal);

	Vector3 normal;

	float radius = 0.0f;
};
