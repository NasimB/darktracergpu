#include "raytracer.h"
#include "vector3.h"

#include <Windows.h>

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"


Raytracer::Raytracer(SDL_Window* window)
{
	m_window = window;
	m_context = SDL_GL_CreateContext(m_window);

	glewExperimental = GL_TRUE;
	glewInit();

	glClearColor(0, 0, 0, 0);

	// If Vsync is off, comment that
	//SDL_GL_SetSwapInterval(1);

	if (!shader.Init("screen"))
	{
		MessageBoxA(NULL, "Failed to load shader", "Fatal Error", MB_ICONERROR | MB_OK);
	}

	// Create fullscreen triangle
	{
		glGenVertexArrays(1, vao);
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, 0, nullptr, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	glBindVertexArray(vao[0]);
	glBindVertexBuffer(0, vbo, 0, 0);

	// Enable alpha blending (to reduce noise when the camera is static)
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	// Create texture
	{
		glGenTextures(1, &m_texture);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, static_cast<GLsizei>(width), static_cast<GLsizei>(height), 0, GL_RGBA, GL_FLOAT, NULL);
		glBindImageTexture(0, m_texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);
	}
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);

#ifdef _DEBUG
	OutputDebugStringA("===========================\n");
	OutputDebugStringA("GPU compute capabilities:\n");
	int work_grp_cnt[3];
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_grp_cnt[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &work_grp_cnt[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &work_grp_cnt[2]);
	std::string str = "\tMax global (total) work group size x:" + std::to_string(work_grp_cnt[0]) + " y:" + std::to_string(work_grp_cnt[1]) + " z:" + std::to_string(work_grp_cnt[2]) + "\n";
	OutputDebugStringA(str.c_str());

	int work_grp_size[3];
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &work_grp_size[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &work_grp_size[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &work_grp_size[2]);
	str = "\tMax local (in one shader) work group sizes x:" + std::to_string(work_grp_size[0]) + " y:" + std::to_string(work_grp_size[1]) + " z:" + std::to_string(work_grp_size[2]) + "\n";
	OutputDebugStringA(str.c_str());

	int work_grp_inv;
	glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &work_grp_inv);
	str = "\tMax local work group invocations: " + std::to_string(work_grp_inv) + "\n";
	OutputDebugStringA(str.c_str());
	OutputDebugStringA("===========================\n");
#endif // _DEBUG

	// Compile compute shader
	{
		std::string fileContent = Shader::ReadFile("compute.comp");
		char* sourceCode = const_cast<char*>(fileContent.c_str());

		ray_shader = glCreateShader(GL_COMPUTE_SHADER);
		glShaderSource(ray_shader, 1, &sourceCode, NULL);
		glCompileShader(ray_shader);
		// check for compilation errors as per normal here
		int wasCompiled = 0;
		glGetShaderiv(ray_shader, GL_COMPILE_STATUS, &wasCompiled);
		if (wasCompiled == 0)
		{
			std::string str = "=======================================\n";
			str += "Shader compilation failed :\n";

			// Find length of shader info log
			int maxLength;
			glGetShaderiv(ray_shader, GL_INFO_LOG_LENGTH, &maxLength);

			// Get shader info log
			char* shaderInfoLog = new char[maxLength];
			glGetShaderInfoLog(ray_shader, maxLength, &maxLength, shaderInfoLog);

			// Print shader info log
			str += "\tError info : " + std::string(shaderInfoLog) + "\n";

			str += "=======================================\n\n";
			OutputDebugStringA(str.c_str());

			delete[] shaderInfoLog;
		}

		ray_program = glCreateProgram();
		glAttachShader(ray_program, ray_shader);
		glLinkProgram(ray_program);
		// check for linking errors and validate program as per normal here
		int isLinked = 0;
		glGetProgramiv(ray_program, GL_LINK_STATUS, static_cast<int *>(&isLinked));
		if (isLinked == 0)
		{
			std::string str = "=======================================\n";
			str += "Shader linking failed !\n";

			// Find length of shader info log
			int maxLength;
			glGetProgramiv(ray_program, GL_INFO_LOG_LENGTH, &maxLength);

			str += "Info Length : " + std::to_string(maxLength) + "\n";

			// Get shader info log
			char* shaderProgramInfoLog = new char[maxLength];
			glGetProgramInfoLog(ray_program, maxLength, &maxLength, shaderProgramInfoLog);

			str += "Linker error message: " + std::string(shaderProgramInfoLog) + "\n";

			str += "=======================================\n\n";
			OutputDebugStringA(str.c_str());

			/* Handle the error in an appropriate way such as displaying a message or writing to a log file. */
			/* In this simple program, we'll just leave */
			delete[] shaderProgramInfoLog;
		}
	}

	timeVariableLoc = glGetUniformLocation(ray_program, "time");
	camPosVariableLoc = glGetUniformLocation(ray_program, "cameraPosition");

	zeroZeroVariableLoc = glGetUniformLocation(ray_program, "zeroZero");
	zeroOneVariableLoc = glGetUniformLocation(ray_program, "zeroOne");
	oneZeroVariableLoc = glGetUniformLocation(ray_program, "oneZero");
	oneOneVariableLoc = glGetUniformLocation(ray_program, "oneOne");

	Scene scene = BuildObjectBuffer();

	glGenBuffers(1, &sceneSSBO);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, sceneSSBO);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(Scene), &scene, GL_DYNAMIC_COPY);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	GLuint block_index = glGetProgramResourceIndex(ray_program, GL_SHADER_STORAGE_BLOCK, "Scene");

	GLuint ssbo_binding_point_index = 2;
	glShaderStorageBlockBinding(ray_program, block_index, ssbo_binding_point_index);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, ssbo_binding_point_index, sceneSSBO);
}


Raytracer::~Raytracer()
{
	glDetachShader(ray_program, ray_shader);
	glDeleteProgram(ray_program);
	glDeleteShader(ray_shader);

	glDeleteTextures(1, &m_texture);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, vao);
	shader.CleanUp();
	SDL_GL_DeleteContext(m_context);
}


Raytracer::Scene Raytracer::BuildObjectBuffer()
{
	Scene scene;
	memset(&scene, 0, sizeof(Scene));

	/*

	scene.num_spheres = 9;

	scene.spheres[0] = {
		{ 0.0f, -2003.0f, 0.0f },
		2000.0f,
		{ 0.5f, 0.5f, 0.5f, 1.0f },
		{ 0.0f, 0.0f, 0.0f, 0.0f }
	};

	scene.spheres[1] = {
		{ 0.0f, 2003.0f, 0.0f },
		2000.0f,
		{ 0.5f, 0.5f, 0.5f, 1.0f },
		{ 0.0f, 0.0f, 0.0f, 0.0f }
	};

	scene.spheres[2] = {
		{ -2003.0f, 0.0f, 0.0f },
		2000.0f,
		{ 0.5f, 0.5f, 0.5f, 1.0f },
		{ 0.0f, 0.0f, 0.0f, 0.0f }
	};

	scene.spheres[3] = {
		{ 2003.0f, 0.0f, 0.0f },
		2000.0f,
		{ 0.5f, 0.5f, 0.5f, 1.0f },
		{ 0.0f, 0.0f, 0.0f, 0.0f }
	};

	scene.spheres[4] = {
		{ 0.0f, 0.0f, -2003.0f },
		2000.0f,
		{ 0.5f, 0.5f, 0.5f, 1.0f },
		{ 0.0f, 0.0f, 0.0f, 0.0f }
	};

	scene.spheres[5] = {
		{ 0.0f, 0.0f, 2020.0f },
		2000.0f,
		{ 0.5f, 0.5f, 0.5f, 1.0f },
		{ 0.0f, 0.0f, 0.0f, 0.0f }
	};

	scene.spheres[6] = {
		{ 1.0f, -2.0f, 0.0f },
		1.0f,
		{ 1.0f, 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 0.0f, 0.0f }
	};

	scene.spheres[7] = {
		{ -1.0f, -2.0f, -2.0f },
		1.0f,
		{ 0.0f, 1.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 0.0f, 0.0f }
	};

	scene.spheres[8] = {
		{ 0.0f, 3.0f, 0.0f },
		1.0f,
		{ 0.0f, 1.0f, 0.0f, 1.0f },
		{ 5.0f, 5.0f, 5.0f, 0.0f }
	};*/


	std::string inputfile = "teapot.obj"; // "Low-Poly-Racing-Car.obj";
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err;
	bool ret = LoadObj(&attrib, &shapes, &materials, &err, inputfile.c_str());

	if (!ret)
		return scene;

	if (!err.empty()) { // `err` may contain warning message.
		std::cerr << err << std::endl;
	}


	int tri_count = 0;

	for (size_t s = 0; s < shapes.size(); s++) 
	{
		// Loop over faces(polygon)
		size_t index_offset = 0;
		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) 
		{
			int fv = shapes[s].mesh.num_face_vertices[f];

			

			tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + 0];
			int vertex_index = 3 * idx.vertex_index;
			tinyobj::real_t vx1 = attrib.vertices[vertex_index + 0];
			tinyobj::real_t vy1 = attrib.vertices[vertex_index + 1];
			tinyobj::real_t vz1 = attrib.vertices[vertex_index + 2];

			idx = shapes[s].mesh.indices[index_offset + 1];
			vertex_index = 3 * idx.vertex_index;
			tinyobj::real_t vx2 = attrib.vertices[vertex_index + 0];
			tinyobj::real_t vy2 = attrib.vertices[vertex_index + 1];
			tinyobj::real_t vz2 = attrib.vertices[vertex_index + 2];

			idx = shapes[s].mesh.indices[index_offset + 2];
			vertex_index = 3 * idx.vertex_index;
			tinyobj::real_t vx3 = attrib.vertices[vertex_index + 0];
			tinyobj::real_t vy3 = attrib.vertices[vertex_index + 1];
			tinyobj::real_t vz3 = attrib.vertices[vertex_index + 2];

			Vector3 N;
			{
				Vector3 v0(vx1, vy1, vz1);
				Vector3 v1(vx2, vy2, vz2);
				Vector3 v2(vx3, vy3, vz3);

				Vector3 v0v1 = v1 - v0;
				Vector3 v0v2 = v2 - v0;
				// no need to normalize
				N = v0v1.cross(v0v2);
			}


			float red = 0.5f;
			float green = 0.5f;
			float blue = 0.5f; 

			if(!materials.empty())
			{
				int material_id = shapes[s].mesh.material_ids[f];
				tinyobj::material_t& material = materials[material_id];
				red = static_cast<float>(material.diffuse[0]);
				green = static_cast<float>(material.diffuse[1]);
				blue = static_cast<float>(material.diffuse[2]);
			}

			scene.triangles[tri_count] = {
				{ vx1,  vy1, vz1 },
				0.0f,
				{ vx2, vy2, vz2 },
				0.0f,
				{ vx3,  vy3, vz3 },
				0.0f,
				{ red, green, blue, 1.0f },
				{ N.x, N.y, N.z },
				0.0f,
				{ 0.0f, 0.0f, 0.0f, 0.0f }
			};

			tri_count++;

			// Loop over vertices in the face.
			/*for (size_t v = 0; v < fv; v++) {
				// access to vertex
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
				tinyobj::real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
				tinyobj::real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
				tinyobj::real_t vz = attrib.vertices[3 * idx.vertex_index + 2];
				tinyobj::real_t nx = attrib.normals[3 * idx.normal_index + 0];
				tinyobj::real_t ny = attrib.normals[3 * idx.normal_index + 1];
				tinyobj::real_t nz = attrib.normals[3 * idx.normal_index + 2];
				tinyobj::real_t tx = attrib.texcoords[2 * idx.texcoord_index + 0];
				tinyobj::real_t ty = attrib.texcoords[2 * idx.texcoord_index + 1];
				// Optional: vertex colors
				// tinyobj::real_t red = attrib.colors[3*idx.vertex_index+0];
				// tinyobj::real_t green = attrib.colors[3*idx.vertex_index+1];
				// tinyobj::real_t blue = attrib.colors[3*idx.vertex_index+2];
			}*/

			index_offset += fv;

			// per-face material
			//shapes[s].mesh.material_ids[f];
		}
	}


	float half_size = 15.0f;
	float emission_power = 20.0f;
	float height = 50.0f;

	scene.triangles[tri_count] = {
		{ -half_size, height, half_size },
		0.0f,
		{ -half_size, height, -half_size },
		0.0f,
		{ half_size, height, -half_size },
		0.0f,
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		{ 0.0f, -1.0f, 0.0f },
		0.0f,
		{ 1.0f * emission_power, 1.0f * emission_power, 1.0f * emission_power, 0.0f }
	};

	scene.triangles[tri_count + 1] = {
		{ half_size, height, -half_size },
		0.0f,
		{ half_size, height, half_size },
		0.0f,
		{ -half_size, height, half_size },
		0.0f,
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		{ 0.0f, -1.0f, 0.0f },
		0.0f,
		{ 1.0f * emission_power, 1.0f * emission_power, 1.0f * emission_power, 0.0f }
	};

	scene.num_triangles = tri_count + 2;

	return scene;
}


void Raytracer::Render(float deltaTime)
{
	static Vector3 old_camera_position = camera.position;

	camera.ComputeCornerDirections(width, height);

	BuildObjectBuffer();

	// launch compute shaders
	{
		glUseProgram(ray_program);

		glUniform1ui(timeVariableLoc, SDL_GetTicks());

		glUniform3f(camPosVariableLoc, camera.position.x, camera.position.y, camera.position.z);

		glUniform3f(zeroZeroVariableLoc, camera.zeroZero.x, camera.zeroZero.y, camera.zeroZero.z);
		glUniform3f(zeroOneVariableLoc, camera.zeroOne.x, camera.zeroOne.y, camera.zeroOne.z);
		glUniform3f(oneZeroVariableLoc, camera.oneZero.x, camera.oneZero.y, camera.oneZero.z);
		glUniform3f(oneOneVariableLoc, camera.oneOne.x, camera.oneOne.y, camera.oneOne.z);

		glDispatchCompute(width / 8, height / 8, 1);
	}

	// make sure writing to image has finished before read
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	// draw fullscreen triangle
	{
		//glClear(GL_COLOR_BUFFER_BIT);

		float delta = (camera.position - old_camera_position).length();

		shader.UseProgram();

		GLuint loc = glGetUniformLocation(shader.shaderProgram, "blend");
		glUniform1f(loc, max(min(delta / 0.1f, 1.0f), 0.1f));

		glDrawArrays(GL_TRIANGLES, 0, 3);
	}

	SDL_GL_SwapWindow(m_window);

	old_camera_position = camera.position;
}
