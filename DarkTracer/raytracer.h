#pragma once

#include <SDL.h>
#include <GL\glew.h>

#include "shader.h"
#include "camera.h"

/*#include "radeon_rays.h"
#include "radeon_rays_cl.h"
#include "CLW.h"*/

class Raytracer
{
public:
	Raytracer(SDL_Window* window);
	~Raytracer();

	
	void Render(float deltaTime);

	static const unsigned int width = 1280;
	static const unsigned int height = 720;

	Camera camera;

private:
	struct SphereStruct
	{
		float position[3];
		float radius;
		float color[4];
		float emission[4];
	};

	struct TriangleStruct
	{
		GLfloat v1[3];
		GLfloat dummy_1;
		GLfloat v2[3];
		GLfloat dummy_2;
		GLfloat v3[3];
		GLfloat dummy_3;
		GLfloat color[4];
		GLfloat normal[3];
		GLfloat dummy_4;
		GLfloat emission[4];
	};

	struct Scene
	{
		//SphereStruct spheres[16];
		TriangleStruct triangles[2048];
		//int num_spheres;
		int num_triangles;
	};

	static Scene BuildObjectBuffer();

	SDL_Window* m_window;
	SDL_GLContext m_context;
	Shader shader;

	GLuint vao[1];
	GLuint vbo;

	GLuint m_texture;

	GLuint ray_shader;
	GLuint ray_program;

	GLint timeVariableLoc;

	GLint camPosVariableLoc;

	GLint zeroZeroVariableLoc;
	GLint zeroOneVariableLoc;
	GLint oneZeroVariableLoc;
	GLint oneOneVariableLoc;

	GLuint sceneSSBO;


	/*RadeonRays::IntersectionApi* g_api;

	//CL data
	CLWContext g_context;
	CLWProgram g_program;
	CLWBuffer<float> g_positions;
	CLWBuffer<float> g_normals;
	CLWBuffer<int> g_indices;
	CLWBuffer<float> g_colors;
	CLWBuffer<int> g_indent;*/
};
