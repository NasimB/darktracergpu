#version 430

// Some drivers require this line to function properly
precision highp float;
 
uniform sampler2D srcTex;
in vec2 texCoord;

uniform float blend;
 
void main(void) 
{
	vec3 c = texture(srcTex, texCoord).rgb;
    gl_FragColor = vec4(c.x, c.y, c.z, blend);
}
