#include "sphere.h"

Sphere::Sphere() :
	radius(1.0f)
{
}


Sphere::Sphere(Vector3 position, float radius)
{
	this->position = position;
	this->radius = radius;
}


Sphere::Sphere(Vector3 position, float radius, Vector3 color)
{
	this->position = position;
	this->radius = radius;
	this->color = color;
}


Sphere::~Sphere()
{
}

bool solveQuadratic(const float &a, const float &b, const float &c, float &x0, float &x1)
{
	float discr = b * b - 4 * a * c;
	if (discr < 0) return false;
	else if (discr == 0) x0 = x1 = -0.5f * b / a;
	else {
		float q = (b > 0) ?
			-0.5f * (b + sqrt(discr)) :
			-0.5f * (b - sqrt(discr));
		x0 = q / a;
		x1 = c / q;
	}
	if (x0 > x1) std::swap(x0, x1);

	return true;
}


bool Sphere::Intersects(Ray ray, float &near, Vector3 &hitpoint, Vector3 &normal)
{
	float t0, t1; // solutions for t if the ray intersects 
#if 0 
				  // geometric solution
	Vec3f L = center - orig;
	float tca = L.dotProduct(dir);
	// if (tca < 0) return false;
	float d2 = L.dotProduct(L) - tca * tca;
	if (d2 > radius2) return false;
	float thc = sqrt(radius2 - d2);
	t0 = tca - thc;
	t1 = tca + thc;
#else 
				  // analytic solution
	Vector3 L = ray.origin - position;
	float a = ray.direction.dot(ray.direction);
	float b = 2 * ray.direction.dot(L);
	float c = L.dot(L) - (radius * radius);
	if (!solveQuadratic(a, b, c, t0, t1)) return false;
#endif 
	if (t0 > t1) std::swap(t0, t1);

	if (t0 < 0) 
	{
		t0 = t1; // if t0 is negative, let's use t1 instead 
		if (t0 < 0) return false; // both t0 and t1 are negative 
	}

	if (t0 > near)
		return false;

	near = t0;

	hitpoint = ray.origin + t0 * ray.direction;
	normal = (hitpoint - position).normalize();
	return true;
}
