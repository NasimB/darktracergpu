#pragma once

#include "object.h"

class Sphere : public Object
{
public:
	Sphere();
	Sphere(Vector3 position, float radius);
	Sphere(Vector3 position, float radius, Vector3 color);
	~Sphere();

	// Returns true if it intersects with the ray and is the closest to its origin, false otherwise.
	virtual bool Intersects(Ray ray, float &near, Vector3 &hitpoint, Vector3 &normal);

	float radius;
};
